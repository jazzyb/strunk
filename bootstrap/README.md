# Introduction to Strunk

**Strunk** is a [literate programming](http://literateprogramming.com/)
compiler.  The intent of Strunk is to take source documentation written in
markdown (like this) and to compile that source into both a "tangled" python
module and "weaved" HTML documentation for that module.  In time, perhaps,
Strunk will be able to compile to many other programming (and markup)
languages.

The project name, Strunk, is named for the author of *The Elements of Style*:

> Vigorous writing is concise. A sentence should contain no unnecessary words,
> a paragraph no unnecessary sentences, for the same reason that a drawing
> should have no unnecessary lines and a **[program] no unnecessary [code]**.
> This requires not that the writer make all his sentences short, or that he
> avoid all detail and treat his subjects only in outline, but that he make
> every word tell.
>
> -- William Strunk Jr.

I hope that by putting the communication aspect of programming first -- the
*Why?* before the *How?* -- I will write better, less error-prone programs.
And what better way to test this hypothesis than to
[dogfood](https://en.wikipedia.org/wiki/Self-hosting_(compilers)) Strunk
itself?

> Thus, I came to the conclusion that the designer of a new system must not
> only be the implementor and the first large-scale user; the designer should
> also write the first user manual.  The separation of any of these four
> components would have hurt TeX significantly.  If I had not participated
> fully in all these activities, literally hundreds of improvements would
> never have been made, because I would never have thought of them or
> perceived why they were important.
>
> -- Donald E. Knuth

# Introduction to the bootstrap

The first problem we run into is figuring out how to bootstrap the initial
compiler.  I have decided to follow the example of Knuth, who wrote the first
[WEB](https://en.wikipedia.org/wiki/WEAVE) source code and then manually
converted it into Pascal until his bootstrap worked.  Likewise, this document
will serve as the first version of the Strunk compiler.

## Requirements

The initial code should have the minimal functionality to tangle the markdown
you are reading into a working python script without burdening me with too
much manual labor during the copy-paste bootstrap process.

Let's start with the following:

1. [x] The bootstrap will only implement "tangle" -- compiling the fenced code
   blocks in a single markdown file into python source code.
1. [x] Code blocks will be fenced in with three "tic" marks:
   ~~~
   ``` python
   def function() -> None:
       # python code goes here
       ...
   ```
   ~~~
   The language of the code block (`python` in this case) is optional, but
   will help with the display of the source code on sites like GitLab/GitHub.
1. [x] Each code block that is meant to be compiled to python must begin
   with a python comment containing one of the following directives:
   1. [x] `# .create "filename" [permissions]`
      - Writes this block to `filename`
      - [x] Optionally will create that file with `permissions` specified in
	octal mode
      - [x] Directories in the file path are created as needed
      - Example: `# .create "script/bootstrap.py" 0755`
   2. [x] `# .define "name or short description"`
      - Defines a code block identified by the string following `.define`
      - [x] The identifier is case-insensitive
      - Example: `# .define "Calculate the Nth Fibinacci number"`
   3. [x] `# .continue "name or short description"`
      - Appends this code block to the code block previously defined with this
	identifier
      - [x] The identifier only needs to contain enough of the string to
	uniquely identify the block
      - Example: `# .continue "calculate the nth fib..."`
1. [x] Code blocks lacking any of the above directives in their *first line*
   will not be compiled to python.
1. [x] Code blocks may additionally contain inline comments with the following
   directive:
   1. [x] `# .include "name or short description"`
      - Inserts the block identified with the given description into the
	current code block
      - [x] Recursive includes shall throw an error
      - [x] Like `.continue`, `.include` only needs to specify enough of the
	identifier to be unique
      - [x] Since python is sensitive to whitespace indentation, the included
	block shall be indented to the level of the `.include` comment
      - Example:
        ``` python
        def fib(n: int) -> int:
            # .include "calculate..."
        ```

From these requirements, you might begin to see how a single file -- or even a
whole module -- could be constructed, beginning with a high-level description
of a script:

~~~ python
# .create "bootstrap.py" 0755

# .include "imports"

# .include "functionality A"

# .include "functionality B"

...

if __name__ == "__main__":
    # .include "main"
~~~

This would then be followed by defining each block in whatever order seemed
best.  One could also apply a bottom-up approach by discussing the bare
functionality first and then expanding these pieces into higher levels of
usage until arriving at the definition of the final file.  (We'll follow the
latter approach.)

The point is that the code is now written to convey the author's reasoning
rather than the dictates of a compiler or interpreter.  The code is written
to be *read* rather than merely processed.

# Bootstrap implementation

## Finding code blocks

Per the previous requirements, we'll only match fenced code blocks that meet
the following start and end patterns:

``` python
# .define "Imports"
import re
...
```
``` python
# .define "Globals"
FENCE_RE = re.compile(r'^\s*```')
...
```

Note that any words following the opening "fence" will be ignored, and the
start and end fences need not have identical indentation.

For each line of our source file, if the line matches `FENCE_RE`, then we
start a new code block.  We then append each successive line to the current
code block until we hit another match.

``` python
# .continue "Imports"
from typing import List
...
```
``` python
# .define "Read markdown code blocks"
def read_markdown(markdown_content: str) -> None:
    lines = markdown_content.splitlines()[::-1]
    while lines:
        line = lines.pop()
        if FENCE_RE.match(line) is None:
            continue

        block: List[str] = []
        while lines:
            line = lines.pop()
            if FENCE_RE.match(line) is not None:
                break
            block.append(line)
        process_code_block(block)  # ...to be defined later...
```

Note that this code will treat all lines between a beginning fence and EOL as
a code block as well -- which matches [the CommonMark
spec](https://spec.commonmark.org/0.30/#fenced-code-blocks).

## Processing directives

We have four directives to identify:  `create`, `define`, `continue`, and
`include`.  The first three *must* be in a comment of the first line of the
code block.  Otherwise, we'll throw the block away.  If the directive is
`create`, we can store the lines in a global `FILES` dict, keyed on the
filename -- with a separate `PERMS` dict to store the optional file
permissions.  Likewise, we can store `define` blocks in a global `BLOCKS`
dict.  And we will append `continue` lines to an already existing block.

These directives seem fairly straight-forward to identify:

``` python
# .continue "Imports"
import re
from typing import Sequence
...
```
``` python
# .continue "Globals"
CREATE_RE = re.compile(r'^\s*#\s*\.create\s+"(?P<filename>.+)"(\s+(?P<permissions>[0-7]+))?')
DEFINE_RE = re.compile(r'^\s*#\s*\.define\s+"(?P<identifier>.+)"')
CONTINUE_RE = re.compile(r'^\s*#\s*\.continue\s+"(?P<identifier>.+)"')
...
```
``` python
# .define "Process code blocks"
def process_code_block(block: Sequence[str]) -> None:
    match = CREATE_RE.match(block[0])
    if match is not None:
        # .include "Process new file"
        return
    match = DEFINE_RE.match(block[0])
    if match is not None:
        # .include "Process new block"
        return
    match = CONTINUE_RE.match(block[0])
    if match is not None:
        # .include "Continue previous block"
        return
    return  # else do nothing with the block
```

And for each match, we'll place them into the appropriate global structure:

``` python
# .continue "Imports"
from typing import Dict, Sequence
...
```
``` python
# .continue "Globals"
FILES: Dict[str, Sequence[str]] = {}
PERMS: Dict[str, int] = {}
BLOCKS: Dict[str, Sequence[str]] = {}
...
```
``` python
# .define "Process new file"
filename, perms = match.group('filename', 'permissions')
FILES[filename] = block
if perms is not None:
    PERMS[filename] = int(perms, 8)
```
``` python
# .define "Process new block"
BLOCKS[match.group('identifier').lower()] = block
```
``` python
# .define "Continue previous block"
identifier = find_block(match.group('identifier').lower())
BLOCKS[identifier].extend(block)
```

Since we want to be able to include blocks that might be defined *after* the
include -- exemplified by the "process code..." block above -- we will wait to
process `include` directives until we have processed all the code blocks.

Note that for the "continue..." block above, we are manually looking up the
identifier string with `find_block()` because our requirements state that the
identifiers for `continue` and `include` only need to be long enough to
uniquely identify the block:

``` python
# .continue "Imports"
import re
...
```
``` python
# .define "Find block by identifier"
def find_block(blkid: str) -> str:
    if blkid in BLOCKS:
        return blkid  # no truncation
    for key in BLOCKS:
        if key.startswith(re.sub(r'\.+$', '', blkid)):
            return key
    raise RuntimeError('no block found with the name "{}"'.format(blkid))
```

Note that "find block..." will return the identifier of the *first* match --
not necessarily the only unique one -- but this is sufficient for now.

## Caveats about the code style

I should take a moment here to address the code style.

First, the astute reader will notice that I imported the `re` module multiple
times.  One should never see this in a regular python script.  However, I find
that with literate programming, this is actually good practice.  Let's say
that I didn't re-import `re`.  Had I later gone back and edited the first
block to remove my reliance on `re` and its importation, I would then have to
remember to find the next place this module was referenced and import `re`
there.  But by importing the module in every place it's used, I'm free to edit
blocks as I need and not worry as much about how their contents will affect
other blocks.  Python is nice in this respect because `import` is idempotent.

The second vice you might have noticed is my (over-)reliance on global
variables and parsing via regular expressions.  I don't like either myself, but
since I'm only trying to complete a working bootstrap script, I consider it a
necessary evil to getting to a minimally viable program.  Besides, I can more
easily refactor Strunk into a proper python module (with tests even!) once the
bootstrap is in place.

## Processing the `include` directive

Once all the code blocks have been saved, we come to the `include` directive.
Since we have to be sensitive to python's whitespace indentation, we have to
keep track of the amount of indentation to add to included blocks:

``` python
# .continue "Imports"
import re
...
```
``` python
# .continue "Globals"
INCLUDE_RE = re.compile(r'^(?P<indentation>\s*)#\s*\.include\s+"(?P<identifier>.+)"')
...
```

Since we can have any number of nested includes, our function for getting the
next line should be recursive.  In order to avoid a cycle of recursive
includes, we also have to keep track of blocks we have already processed:

``` python
# .continue "Imports"
from typing import Generator, Sequence, Set
...
```
``` python
# .define "Process nested includes"
def process_lines(lines: Sequence[str], prev: Set[str] = set()) -> Generator[str, None, None]:
    for line in lines:
        match = INCLUDE_RE.match(line)
        if match is None:
            yield line
        else:
            indent, blkid = match.group('indentation', 'identifier')
            blkid = find_block(blkid.lower())
            if blkid in prev:
                raise RuntimeError('recursive cycle in block "{}"'.format(blkid))
            for line in process_lines(BLOCKS[blkid], prev | set((blkid,))):
                yield indent + line
```

## Creating the files

Now that all the directive handling is finished and we've collected our code
blocks, we can walk the `FILES` dict and write the code blocks to the
necessary files -- ensuring that any directories in the path exist.  We'll
also write a header to the top of every file to mark how the final script was
generated:

``` python
# .continue "Imports"
from datetime import datetime, timezone
from platform import python_implementation, python_version
import os
```
``` python
# .continue "Globals"
STRUNK_HEADER = """#
# !!! PLEASE DO NOT EDIT THIS FILE !!!
#
# This file was automatically generated by the Strunk bootstrap script
#     running {} ({})
#     on {}.
#
""".format(python_implementation(), python_version(), datetime.now(timezone.utc))
```
``` python
# .define "Write blocks to files"
def write_files() -> None:
    for filename in FILES:
        dirname, basename = os.path.split(filename)
        if dirname:
            os.makedirs(dirname, exist_ok=True)
        lines = STRUNK_HEADER.splitlines()
        lines.extend(line for line in process_lines(FILES[filename]))
        with open(filename, mode='w', newline='') as f:
            f.write(os.linesep.join(lines))
        if filename in PERMS:
            os.chmod(filename, PERMS[filename])
```

Note the `newline` option to `open()`.  From the
[official documentation](https://docs.python.org/3/library/functions.html#open):

> When writing output to the stream, if newline is None, any '\n' characters
> written are translated to the system default line separator, os.linesep. If
> newline is '' or '\n', no translation takes place. If newline is any of the
> other legal values, any '\n' characters written are translated to the given
> string.

This means that without the `newline=''` option on Windows, python will
replace the default `\r\n` line endings with `\r\r\n`.  This results in an
output file that "works" but is *hideous*.

## Putting it all together

Now that we have all the pieces, we can define the skeleton of the bootstrap:

``` python
# .create "bootstrap/tangle.py" 0755

# .include "Imports"

# .include "Globals"

# .include "Find block..."

# .include "Read markdown..."

# .include "Process code blocks"

# .include "Process nested includes"

# .include "Write blocks..."

if __name__ == '__main__':
    import sys

    source = sys.argv[1]
    with open(source, 'r') as f:
        contents = f.read()
    read_markdown(contents)
    write_files()
```

Now I can manually copy-paste the above code to `tangle.py` and run it with:

``` bash
$ python tangle.py bootstrap/README.md
```

And it will produce the file `bootstrap/tangle.py`.  Running that new script
against `bootstrap/README.md` should produce the same file again (with a
different timestamp in the header).

The command:

``` bash
$ diff -w tangle.py bootstrap/tangle.py
```

will show any changes between the two files.  Once I've verified that the only
change is the header, then I know it works.

# Conclusions

One of my favorite parts to code was in the [Processing
directives](#processing-directives) section.  Separating the main
`process_code_block()` function from the processing of each directive in the
if-conditions felt really clean.  Of course, I could have split them into
separate functions and accomplished the same thing.  But being able to
intersperse the blocks with a linear narrative describing what I was doing
made much more sense than my typical code organization.

I was also surprised, particularly in that same section, how easily I embraced
global variables.  When you can define them exactly where you use them, they
don't feel as unwieldy.  Perhaps, they would become so on a larger project,
but for this bootstrap code they just felt natural and clearer than passing
arguments around.

I didn't bother to write tests for this script, but I'm a bit worried how
testing would fit into a larger project.  There's usually so much setup,
teardown, and mocking involved in unittesting a non-trivial project that I
doubt it would fit in a document like this one without bringing the narrative
to a crawl.  And the narrative is kind of the point.

Perhaps, the tests could be written separately in normal code, or a different
framework could be used that suited literate programming better?  It seems
like the right testing framework would be suited to literate programming by
allowing you to place your tests right next to the code.  I guess I'll just
have to continue experimenting.

Literate programming feels like it takes longer than regular programming, but
I was amazed at how few bugs I encountered once I got the program running.
Having to explain my decisions to an audience made me make sure that I knew
what the code was doing and that it was doing it in the most straightforward
way possible.  If anything I probably spent more time explaining my code than
writing it.  I would not try to run a software business with literate
programming.
